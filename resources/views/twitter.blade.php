@extends('layouts.main')

@section('content')
    <h2 class="title">Laravel 5 - Twitter API</h2>

    <table class="table table-striped" id="mytable">
        <thead>
        <tr>
            <th width="50px">#</th>
            <th>Twitter Id</th>
            <th>Message</th>
            <th>Images</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($data))
            @foreach($data as $key => $value)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $value['id'] }}</td>
                    <td>{{ $value['text'] }}</td>
                    <td>
                        @if(!empty($value['extended_entities']['media']))
                            @foreach($value['extended_entities']['media'] as $v)
                                <img src="{{ $v['media_url_https'] }}" style="width:100px;">
                            @endforeach
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="6">There are no data.</td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('scripts')
    <script src="js/twitter.js"></script>
@endsection